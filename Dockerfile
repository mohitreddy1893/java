FROM maven:3.6.1-jdk-8 As javabuilder
MAINTAINER "MO"
WORKDIR /app
RUN git clone https://gitlab.com/ot-interview/javaapp.git && \ 
cd javaapp && \ 
mvn clean install
FROM tomcat:8.0-alpine
COPY --from=javabuilder /app/javaapp/target/Spring3HibernateApp.war /usr/local/tomcat/webapps
EXPOSE 8080
CMD ["catalina.sh","run"]


